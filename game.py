from random import randint

name = input("Hi! What is your name? ")

#tries = [1, 2, 3, 4, 5]
#creating the limit of attempts with a list
#this could have been replaced with a simple range function
    #for attempt in range(5)


for attempt in range(5):
    if attempt >= 2 and answer == 'earlier':
        mon_guess = randint(1, mon_guess)
        year_guess = randint(1924, year_guess)
        if mon_guess == 1:
            month = "Jan"
        elif mon_guess == 2:
            month = "Feb"
        elif mon_guess == 3:
            month = "Mar"
        elif mon_guess == 4:
            month = "Apr"
        elif mon_guess == 5:
            month = "May"
        elif mon_guess == 6:
            month = "Jun"
        elif mon_guess == 7:
            month = "Jul"
        elif mon_guess == 8:
            month = "Aug"
        elif mon_guess == 9:
            month = "Sept"
        elif mon_guess == 10:
            month = "Oct"
        elif mon_guess == 11:
            month = "Nov"
        else:
            month = "Dec"
        print('Guess', attempt+1, ":", name, "were you born in", month, year_guess,'?')
        answer = input("yes or no? ")
    #This will check if the user's birthday was earlier than the previous guess
    #reassigns month and year to values that will always be earlier than the previous guess
    #will only apply on attempt 2 and onwards

    elif attempt >= 2 and answer == 'later':
        mon_guess = randint(mon_guess, 12)
        year_guess = randint(year_guess, 2004)
        if mon_guess == 1:
            month = "Jan"
        elif mon_guess == 2:
            month = "Feb"
        elif mon_guess == 3:
            month = "Mar"
        elif mon_guess == 4:
            month = "Apr"
        elif mon_guess == 5:
            month = "May"
        elif mon_guess == 6:
            month = "Jun"
        elif mon_guess == 7:
            month = "Jul"
        elif mon_guess == 8:
            month = "Aug"
        elif mon_guess == 9:
            month = "Sept"
        elif mon_guess == 10:
            month = "Oct"
        elif mon_guess == 11:
            month = "Nov"
        else:
            month = "Dec"
        print('Guess', attempt+1, ":", name, "were you born in", month, year_guess,'?')
        answer = input("yes or no? ")
    #Same deal as the earlier condition

    else:
        mon_guess = randint(1, 12)
        year_guess = randint(1924, 2004)
        if mon_guess == 1:
            month = "Jan"
        elif mon_guess == 2:
            month = "Feb"
        elif mon_guess == 3:
            month = "Mar"
        elif mon_guess == 4:
            month = "Apr"
        elif mon_guess == 5:
            month = "May"
        elif mon_guess == 6:
            month = "Jun"
        elif mon_guess == 7:
            month = "Jul"
        elif mon_guess == 8:
            month = "Aug"
        elif mon_guess == 9:
            month = "Sept"
        elif mon_guess == 10:
            month = "Oct"
        elif mon_guess == 11:
            month = "Nov"
        else:
            month = "Dec"
        print('Guess', attempt+1, ":", name, "were you born in", month, year_guess,'?')
        answer = input("yes or no? ")

    if answer == 'no' and attempt <5:
        print("Drat! lemme try again!")
    elif answer == 'no' and attempt == 5:
        print("I have other things to do. Good bye.")
    elif answer == 'earlier' and attempt == 5:
        print("I have other things to do. Good bye.")
    elif answer == 'later' and attempt == 5:
        print("I have other things to do. Good bye.")
    elif answer == 'yes':
        print("Easy Peasy")
        break
        #breaking the loop because the computer got it right and game not need to continue
    elif answer == 'earlier':
        print("In that case, I will guess an earlier date.")
    elif answer == 'later':
        print("In that case, I will guess a later date")
    else:
        print("invalid answer, try again")
        pass
